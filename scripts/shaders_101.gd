extends Node2D

@onready var sprite = $Sprite2D

#func _process(delta):
	#var TIME = Time.get_ticks_msec() / 1000.0
	#var a = (cos(TIME) + 1.0) * 0.5
	#
	#print("TIME: " + str(TIME))
	#print("a: " + str(a))

func _process(delta):
	if Input.is_action_just_pressed("set_speed"):
		var new_speed = randf_range(1.0, 10.0)
		sprite.material.set_shader_parameter("my_float", new_speed)
		print(new_speed)
	
	if Input.is_action_just_pressed("set_color"):
		var new_color = Vector4(randf(), randf(), randf(), 1.0)
		sprite.material.set_shader_parameter("my_color", new_color)
		print(new_color)
	
	#print(sprite.material.get_shader_parameter("my_int"))
